// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike20.h"
#include "CollisionDetection.h"


// Sets default values
ACollisionDetection::ACollisionDetection()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACollisionDetection::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollisionDetection::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	
	for (FConstPawnIterator iterator = GetWorld()->GetPawnIterator(); iterator; ++iterator) {
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, iterator->Get()->GetName);
	}
}

// Called to bind functionality to input
void ACollisionDetection::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);	
}

//
//void ACollisionDetection::CheckForCollision(ACollisionDetection* obj1, ACollisionDetection* obj2)
//{
//
//}