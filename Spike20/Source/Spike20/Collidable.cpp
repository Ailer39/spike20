// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike20.h"
#include "Collidable.h"


// Sets default values for this component's properties
UCollidable::UCollidable()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCollidable::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UCollidable::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

